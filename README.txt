bookm_application: folder project netbeans
	/libraries: berisi file libraries yang diperlukan
		/sqlite-jdbc-3.20.0.jar:
		  -untuk hubungkan Java dengan SQLite. 
		  -Source: https://bitbucket.org/xerial/sqlite-jdbc/downloads
		/Placeholder.jar:
		  -untuk menambahkan fungsi placeholder kepada JTextField. (Contoh penggunaan: new Placeholder(JTextField1, "Isikan field 1");).
		  -Source: http://plugins.netbeans.org/plugin/70043/placeholder
		/Scalr.jar:
		  -untuk menambahkan fungsi scaling gambar (Contoh penggunaan: Scalr.resize(bufferedImage,120);).
		  -Source: https://github.com/rkalla/imgscalr
	/images: berisi file gambar yang diperlukan aplikasi
		/icon: berisi file icon seperti icon "X"
	/database: berisi file database
	/dist: file jar
	  -disertai folder images dan database

tools: berisi installer untuk alat pembantu yang akan dihapus dari repository setelah dimiliki semua member
	/SourceTreeSetup-2.0.20.1.exe: helper untuk berhubungan dengan bitbucket
	/sqlitestudio-3.1.1.zip: SQLiteStudio, IDE SQLite beserta file SQLite itu sendiri
	/sqlite-tools-win32-x86-3200100.zip: file SQLite tanpa IDE
	  -bila ingin menggunakan IDE SQLiteStudio, pilih "sqlitestudio-3.1.1.zip". bila ingin menggunakan tanpa IDE, pilih "sqlite-tools-win32-x86-3200100.zip"

Notes:
1. Saat melakukan push, berikan catatan yang terstruktur tetapi singkat tentang fitur apa saja yang ditambahkan. Juga berikan informasi di LINE.
2. Bila memerlukan pergantian struktur database, atau data yang krusial jangan lupa sertakan catatan bahwa ada pergantian database saat mengepush.
3. Juga ingat untuk berikan penggantian README.txt, saat ada folder atau baru yang diperlukan untuk berjalannya suatu fungsi.
4. Apabila kesulitan untuk menggabungkan program anda dan program yang ada di repository, lebih baik gunakan folder dengan format "[nama anda]-[tanggal]:[isi fitur]" (Contoh: "aldo-20/10/2017:fungsi edit buku") di root, agar tidak terjadi kehilangan data pada project program yang sudah ada.

#############---VERSION CHANGES---#############
v1.0
1. Penambahan fungsi untuk mengingat path terakhir saat melakukan browse file
	- Pada tambah buku, dan edit buku
2. Pengembalian password awal menjadi "admin"

v0.6
1. Penambahan checkbox dalam melakukan pencarian, pengguna dapat mencari berdasarkan beberapa maupun semua baik dari judul, pengarang, penerbit, sinopsis
2. Pembenahan bug untuk menggunakan icon bintang dalam mengganti peringkat

v0.5.5.2
1. Menggantikan tampilan peringkat dengan Bintang
2. Mengganti font TAB Genre "Tahoma" menjadi "Lithos Pro Reguler" 
	     size "11" menjadi "18"

v0.5.5.1
1. Penambahan checkBox di bawah searching untuk pengkategorian terhadap hasil search
2. Penambahan icon Bintang dan Bintang kosong untuk peringkat
3. Memasukkan folder icon untuk pemilahan font yang akan digunakan selanjutnya

v0.5.5
1. Perubahan tampilan peringkat
	-Dalam bentuk 10 label yang dimasukkan kedalam panel, dengan jumlah peringkat dilambangkan dengan karakter "*" dan sisanya "#"
2. Status baca ditampilkan dengan menggunakan warna border
	-Juga menghilangkan tulisan status baca dalam informasi buku

v0.5.4.3
1. Penambahan checkBox di bawah searching untuk pengkategorian terhadap hasil search
2. Penambahan icon Bintang dan Bintang kosong untuk peringkat

v0.5.4.2
1. Pengubahan settingan look and feel
2. Penambahan fungsi switching icon Login/Logout

v0.5.4.1
1. Penambahan icon untuk login
2. Pemberian warna Background pada setiap Jdialog yang ada

v0.5.4
1. Pembenaran bug searching dan filtering tidak dapat dilakukan bersamaan
	-Penggabungan STATE_PANEL_BUKU, FILTERED dan SEARCHED menjadi SEARCHED_OR_FILTERED
2. Pembenaran bug saat searching buku yang tidak ditampilkan secara urut judul

v0.5.3
1. Penambahan fungsi Edit Genre
	-Penambahan tampilan Edit Genre --> dlgEditGenre (hampir sama dengan Tambah Genre)
	-Penambahan fungsi pada class DatabaseUtil (editGenre(String,String))
2. Pengubahan build.xml agar folder database dan folder images dapat tercopy secara otomatis saat melakukan pembuatan file jar

v0.5.2.1
1. Pengaturan kembali(tata letak dan penambahan tombol) pada tampilan Beranda, Tambah Buku, dan Edit Buku
2. Background sementara menggunakan warna terlebih dahulu
3. Penambahan Jdialog untuk Petunjuk Penggunaan Aplikasi

v0.5
1. Penambahan fungsi Create Genre, dan Delete Genre
	-Penambahan fungsi pada class DatabaseUtil (createGenre(String), dan DeleteGenre(String))
2. Penambahan fungsi filtering tahun terbit dan peringkat
	-Pengubahan fungsi pada frmHalamanUtama (tampilDataBuku(Arraylist<Buku>))

v0.4.7
1. Penambahan fungsi filtering genre
2. Penambahan fungsi mendelete sampul buku dari folder images/buku/ saat data buku dihapus di dalam program

v0.4.6.1
1. Penambahan display tahun terbit bila dimasukkan selain angka

v0.4.6
1. Penambahan state untuk pnlBuku, (0 = kosong, 1 = all, 2 = search)
2. Search sekarang menggunakan, Judul, Penerbit, Pengarang, dan Sinopsis

v0.4.5
1. Update database, menambahkan tabel password
2. Penambahan kelas encryptUtil untuk keperluan hashing password
3. Penambahan fungsi search
4. Penambahan fungsi selectPassword() dan selectBukuBerdasarkanKeyword(String) pada kelas DatabaseUtil
5. Pengubahan fungsi tampilSemuaDataBuku() --> menjadi tampilSemuaBuku(ArrayList<Buku>) agar dapat digunakan dengan lebih fleksibel
6. Penyempurnaan scrolling: sinopsis dimulai dari atas (setValue(0)) dan scroll pnlSemuaBuku lebih cepat (setUnitIncrement(16))

v0.4.1
1. Penampilan genre sudah diperbaiki
2. Mendelete dialog hapus genre
3. Pengubahan tampilan message "Apakah anda yakin?" saat delete buku

v0.4
1. UI sudah digabungkan dengan logika yang telah dibuat saudara Aldo
2. Tampilan dari Database sudah bisa ditampilkan dan sedikit berwarna
3. Penambahan icon Delete pada pojok kanan atas info Buku

v0.3.5
1. Finalisasi fungsi logika CRUD buku, sudah termasuk sudah bisa menyimpan gambar
2. Penambahan efek suara saat error message muncul
3. Class baru antara lain 
   a. SoundUtil untuk keperluan suara, 
   b. ImageFilter untuk filterisasi filechooser hanya bisa menerima gambar, dan
   c. ExtensionUtil untuk keperluan extensi
4. Akan : digabung dengan desain UI Andykj

v0.3.3
1. Penambahan pada fungsi create buku sekarang disertai genre (method createBuku() pada DatabaseUtil dan method btnTambah pada dlgTambahBuku)
2. Untuk sementara genre diawali langsung dengan 3 combobox (mengubah tampilan UI dlgTambahBuku)
3. Pengubahan struktur database, tabel genre hanya memiliki kolom "nama_genre" (tanpa "id_genre"), sehingga "nama_genre" menjadi PK pada tabel genre dan PK sekaligus FK pada tabel daftar_genre
4. Juga terdapat sedikit perubahan pada struktur class buku

v0.3.2
1. Penambahan fungsi create buku
2. Penyempurnaan fungsi read buku

v0.3.1
1. Penambahan fungsi login, termasuk cek login saat tambah genre dan tambah buku.
2. Tampilan option pane (message dialog) di fungsi login, tambah genre, dan tambah buku.
3. Penambahan file jar (v.0.3.1) didalam folder dist disertai copy paste folder images, dan database kedalam folder dist.
4. Penghapusan folder "Yoas-Fungsi Login": sudah diimplementasikan.

v0.3
1. Penyelesaian tampilan dasar untuk: Halaman utama, login, tambah buku, edit buku, hapus buku, tambah genre, edit genre, hapus genre, search+filtering.
2. Penggabungan tampilan desain dengan fungsi tampil semua buku.

v0.2.0

1. Finalisasi struktur database, beserta contoh data
2. Fungsi tampil semua buku (beserta pengambilan data dari database)
3. Penambahan class: Scalr untuk scaling gambar, Buku untuk menampung tabel buku dari database, DatabaseUtil untuk fungsi-fungsi kebutuhan database

v0.1.1

1. Tampilan login
2. Tampilan home

v0.1
1. Hanya berisi file-file awal kosong
2. Tools segera dicopy bila dibutuhkan, karena pada version berikutnya akan dihapus (SQLiteStudio, dan Sourcetree)
3. Menambahkan Placeholder.jar
