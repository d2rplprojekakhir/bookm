/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bookm_application;

/**
 *
 * @author ronal
 */
public class Configuration {
    private boolean isLogin = false;
    
    
    private Configuration() {
    }
    
    public static Configuration getInstance() {
        return ConfigurationHolder.INSTANCE;
    }
    
    private static class ConfigurationHolder {
        private static final Configuration INSTANCE = new Configuration();
    }

    /**
     * @return the isLogin
     */
    public boolean getIsLogin() {
        return isLogin;
    }

    /**
     * @param aIsLogin the isLogin to set
     */
    public void setIsLogin(boolean aIsLogin) {
        isLogin = aIsLogin;
    }
}
