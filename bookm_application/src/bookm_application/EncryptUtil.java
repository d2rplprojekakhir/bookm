/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bookm_application;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 *
 * @author ronal
 */
public class EncryptUtil {
    public static char[] hash(char[] string){
        String encryptedString = "";
        
        String stringToHash = Arrays.toString(string);
        try {
                MessageDigest md5 = MessageDigest.getInstance("MD5");
                md5.update(stringToHash.getBytes());
                byte[] md5Result = md5.digest();
                
                MessageDigest mdSha256 = MessageDigest.getInstance("SHA-256");
                mdSha256.update(md5Result);
                byte[] hashResult = mdSha256.digest();
                
            try {
                encryptedString = new String(hashResult, "UTF-8");
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            }
        }
        catch (NoSuchAlgorithmException e)
        {
            System.out.println(e.getMessage());
        }
        
        return encryptedString.toCharArray();
    }
}
