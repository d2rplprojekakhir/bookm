/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bookm_application;

import javax.swing.JLabel;

/**
 *
 * @author ronal
 */
public class JLabelPeringkat extends JLabel{
    private char type;
    private int idx;    
                
    /**
     *
     * @param type
     */
    public void setType(char type){
        this.type = type;
    }

    /**
     *
     * @return the type
     */
    public char getType(){
        return type;
    }

    /**
     * @return the idx
     */
    public int getIdx() {
        return idx;
    }

    /**
     * @param idx the idx to set
     */
    public void setIdx(int idx) {
        this.idx = idx;
    }
}
