/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bookm_application;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Christnawan
 */
public class DatabaseUtil {
    /**
     * Connect to the bookm.db database
     * @return the Connection object
     */
    public static Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:database/bookm.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
    
    public static ArrayList<Buku> selectSemuaBuku(){
        ArrayList<Buku> bukus = new ArrayList<>();
        
        String sqlSelectBuku = "SELECT * FROM buku ORDER BY 1;";
        try (Connection conn = connect();
             Statement stmtSelectBuku  = conn.createStatement();
             ResultSet rsSelectBuku = stmtSelectBuku.executeQuery(sqlSelectBuku)){
            
            // loop through the result set            
            while (rsSelectBuku.next()) {
                //ambil genre
                String judul = rsSelectBuku.getString("judul");
                String pengarang = rsSelectBuku.getString("pengarang");
                
                ArrayList<String> genres = new ArrayList<>();
                String sqlSelectGenre = "SELECT nama_genre, "
                        + "urutan_masuk "
                        + "FROM daftar_genre "
                        + "WHERE judul = \""+ judul + "\" AND "
                        + "pengarang = \""+ pengarang + "\""
                        + "ORDER BY 2;";
                
                Statement stmtSelectGenre  = conn.createStatement();
                ResultSet rsSelectGenre = stmtSelectGenre.executeQuery(sqlSelectGenre);
                while (rsSelectGenre.next()) {
                    genres.add(rsSelectGenre.getString("nama_genre"));
                }
                
                bukus.add(new Buku(judul, 
                                   pengarang,
                                   rsSelectBuku.getString("penerbit"),
                                   genres,
                                   rsSelectBuku.getInt("peringkat"),
                                   rsSelectBuku.getInt("tahun_terbit"),
                                   rsSelectBuku.getString("sinopsis"),
                                   rsSelectBuku.getString("status"),
                                   rsSelectBuku.getString("path_sampul")
                                   ));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
        return bukus;
    }
    
    public static Buku selectBuku(String judul, String pengarang){
        Buku buku = null;
        String sqlSelectBuku = "SELECT * FROM buku WHERE judul = ? AND pengarang = ?;";
        try (Connection conn = connect();
            PreparedStatement pstmtSelectBuku  = conn.prepareStatement(sqlSelectBuku)){
            pstmtSelectBuku.setString(1,judul);
            pstmtSelectBuku.setString(2, pengarang);
            
            ResultSet rsSelectBuku = pstmtSelectBuku.executeQuery();
            // loop through the result set            
            while (rsSelectBuku.next()) {
                
                ArrayList<String> genres = new ArrayList<>();
                String sqlSelectGenre = "SELECT nama_genre, "
                        + "urutan_masuk "
                        + "FROM daftar_genre "
                        + "WHERE judul = \""+ judul + "\" AND "
                        + "pengarang = \""+ pengarang + "\""
                        + "ORDER BY 2;";
                
                Statement stmtSelectGenre  = conn.createStatement();
                ResultSet rsSelectGenre = stmtSelectGenre.executeQuery(sqlSelectGenre);
                while (rsSelectGenre.next()) {
                    genres.add(rsSelectGenre.getString("nama_genre"));
                }
                
                buku = new Buku(judul, 
                                   pengarang,
                                   rsSelectBuku.getString("penerbit"),
                                   genres,
                                   rsSelectBuku.getInt("peringkat"),
                                   rsSelectBuku.getInt("tahun_terbit"),
                                   rsSelectBuku.getString("sinopsis"),
                                   rsSelectBuku.getString("status"),
                                   rsSelectBuku.getString("path_sampul")
                                   );
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return buku;
    }
    
    public static int selectPeringkat(String judul, String pengarang){
        int peringkat = -1;
        String sqlSelectBuku = "SELECT peringkat FROM buku WHERE judul = ? AND pengarang = ?;";
        try (Connection conn = connect();
            PreparedStatement pstmtSelectBuku  = conn.prepareStatement(sqlSelectBuku)){
            pstmtSelectBuku.setString(1,judul);
            pstmtSelectBuku.setString(2, pengarang);
            
            ResultSet rsSelectBuku = pstmtSelectBuku.executeQuery();
            // loop through the result set            
            while (rsSelectBuku.next()) {
                peringkat = rsSelectBuku.getInt("peringkat");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return peringkat;
    }
    
    public static ArrayList<Buku> selectBukuBerdasarkanKeyword(String keyword,
            boolean isJudul, boolean isPengarang, boolean isPenerbit, boolean isSinopsis) {
        ArrayList<Buku> bukus = new ArrayList<>();
        String sqlSelectBuku = "SELECT * FROM buku WHERE ";
        String conditions[] =
            {
                "judul LIKE '%" + keyword + "%' ",
                "pengarang LIKE '%" + keyword + "%' ",
                "penerbit LIKE '%" + keyword + "%' ",
                "sinopsis LIKE '%" + keyword + "%' "
            };
        boolean isConditions[] = {isJudul,isPengarang,isPenerbit,isSinopsis};
        
        try (Connection conn = connect();){
            
            String conditionFinal = "";

            for(int i = 0; i < conditions.length; i++){
                if(isConditions[i]){
                    if(conditionFinal.equals("")) conditionFinal += conditions[i];
                    else conditionFinal += "OR " + conditions[i];
                }
            }
            
//            System.out.println(conditionFinal);
            
            if(conditionFinal.equals("")) sqlSelectBuku += "1 = 1 ";
            else sqlSelectBuku += conditionFinal;
            
            sqlSelectBuku += "ORDER BY 1;";
            
            Statement stmtSelectBuku = conn.createStatement();
            ResultSet rsSelectBukuBerdasarkanKeyword = 
                    stmtSelectBuku.executeQuery(sqlSelectBuku);
            // loop through the result set            
            while (rsSelectBukuBerdasarkanKeyword.next()) {
                String judul = rsSelectBukuBerdasarkanKeyword.getString("judul");
                String pengarang = rsSelectBukuBerdasarkanKeyword.getString("pengarang");
                
                ArrayList<String> genres = new ArrayList<>();
                String sqlSelectGenre = "SELECT nama_genre, "
                        + "urutan_masuk "
                        + "FROM daftar_genre "
                        + "WHERE judul = \""+ judul + "\" AND "
                        + "pengarang = \""+ pengarang + "\""
                        + "ORDER BY 2;";
                
                Statement stmtSelectGenre  = conn.createStatement();
                ResultSet rsSelectGenre = stmtSelectGenre.executeQuery(sqlSelectGenre);
                while (rsSelectGenre.next()) {
                    genres.add(rsSelectGenre.getString("nama_genre"));
                }
                
                bukus.add(new Buku(judul, 
                                   pengarang,
                                   rsSelectBukuBerdasarkanKeyword.getString("penerbit"),
                                   genres,
                                   rsSelectBukuBerdasarkanKeyword.getInt("peringkat"),
                                   rsSelectBukuBerdasarkanKeyword.getInt("tahun_terbit"),
                                   rsSelectBukuBerdasarkanKeyword.getString("sinopsis"),
                                   rsSelectBukuBerdasarkanKeyword.getString("status"),
                                   rsSelectBukuBerdasarkanKeyword.getString("path_sampul")
                                   ));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return bukus;
    }
    
    public static ArrayList<String> selectSemuaGenre(){
        ArrayList<String> genres = new ArrayList<>();

        String sqlSelectGenre = "SELECT * FROM genre ORDER BY 1;";
        try (Connection conn = connect();
            Statement stmtSelectGenre  = conn.createStatement();
            ResultSet rsSelectGenre = stmtSelectGenre.executeQuery(sqlSelectGenre)){
            // loop through the result set            
            while (rsSelectGenre.next()) {
                String genre = rsSelectGenre.getString("nama_genre");
                
                genres.add(genre);
            }
            
        }
        catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return genres;
    }   
    public static boolean createBuku(String judul, String pengarang, String penerbit,
            ArrayList<String> genres, int peringkat, int tahunTerbit, String sinopsis,
            String status, String pathSampul){
        String sqlInsertBuku = "INSERT INTO buku(judul,pengarang,penerbit,peringkat,"
                + "tahun_terbit,sinopsis,status,path_sampul) VALUES(?,?,?,?,?,?,?,?);";
 
        try (Connection conn = connect();
            PreparedStatement pstmtInsertBuku = conn.prepareStatement(sqlInsertBuku)) {
            pstmtInsertBuku.setString(1, judul);
            pstmtInsertBuku.setString(2, pengarang);
            if(penerbit.equals("Penerbit")) pstmtInsertBuku.setString(3, "");
            else pstmtInsertBuku.setString(3, penerbit);
            if(peringkat <= 0 || peringkat > 10) pstmtInsertBuku.setInt(4, 0);
            else pstmtInsertBuku.setInt(4, peringkat);
            if(tahunTerbit == 0) pstmtInsertBuku.setString(5, "");
            else pstmtInsertBuku.setInt(5, tahunTerbit);
            if(sinopsis.equals("Sinopsis . . .")) pstmtInsertBuku.setString(6, "");
            else pstmtInsertBuku.setString(6, sinopsis);
            pstmtInsertBuku.setString(7, status);
            if(pathSampul.equals("")) pstmtInsertBuku.setString(8, "defaultImage.jpg");
            else pstmtInsertBuku.setString(8, pathSampul);
            int count = pstmtInsertBuku.executeUpdate();
            
            if(count > 0){
                if(genres.size() > 0){
                    String sqlInsertGenre = "INSERT INTO daftar_genre(judul,pengarang,nama_genre,urutan_masuk)"
                            + " VALUES(?,?,?,?);";
                    PreparedStatement pstmtInsertGenre = conn.prepareStatement(sqlInsertGenre);
                    pstmtInsertGenre.setString(1, judul);
                    pstmtInsertGenre.setString(2, pengarang);
                    int i = 1;

                    boolean isSuccessStatus = true;
                    for(String genre: genres){
                        pstmtInsertGenre.setString(3, genre);
                        pstmtInsertGenre.setInt(4, i);
                        
                        int count2 = pstmtInsertGenre.executeUpdate();
                        if(count2 <= 0){
                            isSuccessStatus = false;
                        } 
                        i++;
                    }
                    if(isSuccessStatus){
                        return true;
                    }
                }
                else{
                    return true;
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }
    
    public static boolean createGenre(String namaGenre) {
        String sqlInsertBuku = "INSERT INTO genre(nama_genre) VALUES(?);";
 
        try (Connection conn = connect();
            PreparedStatement pstmtInsertBuku = conn.prepareStatement(sqlInsertBuku)) {
            pstmtInsertBuku.setString(1, namaGenre);
            
            int count = pstmtInsertBuku.executeUpdate();
            
            if(count > 0){
                return true;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    public static boolean deleteBuku(String judul, String pengarang) {
        String sqlSelectGambarSampul = "SELECT path_sampul FROM buku WHERE judul = ? "
                + "AND  pengarang = ?;";
        
        try(Connection conn = connect();
            PreparedStatement pstmtSelectGambarSampul = conn.prepareStatement(sqlSelectGambarSampul)) {
            
            pstmtSelectGambarSampul.setString(1,judul);
            pstmtSelectGambarSampul.setString(2, pengarang);
            
            ResultSet rsSelectGambarSampul = pstmtSelectGambarSampul.executeQuery();
            
            while(rsSelectGambarSampul.next()){
                if(!rsSelectGambarSampul.getString("path_sampul").equals("defaultImage.jpg")){
                    String pathSampul = "images/buku/" + 
                            rsSelectGambarSampul.getString("path_sampul");
                    Path target = Paths.get(pathSampul);

                    try {
                        Files.delete(target);
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            }
            
            String sqlDeleteBuku = "DELETE FROM buku WHERE judul = ? AND  pengarang = ?;";
            PreparedStatement pstmtDeleteBuku = conn.prepareStatement(sqlDeleteBuku);
            
            pstmtDeleteBuku.setString(1, judul);
            pstmtDeleteBuku.setString(2, pengarang);
            
            int count = pstmtDeleteBuku.executeUpdate();
            
            if(count > 0){
                String sqlDeleteGenreBuku = 
                        "DELETE FROM daftar_genre WHERE judul = ? AND  pengarang = ?;";
                
                PreparedStatement pstmtDeleteGenreBuku = 
                        conn.prepareStatement(sqlDeleteGenreBuku);
                pstmtDeleteGenreBuku.setString(1, judul);
                pstmtDeleteGenreBuku.setString(2, pengarang);
                int count2 = pstmtDeleteGenreBuku.executeUpdate();
                if(count2 > -1) return true;
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return false;
    }
    
    public static boolean deleteGenre(String namaGenre){
        String sqlSelectBukuBerdasarkanGenre = "SELECT judul,pengarang,urutan_masuk "
                + "FROM daftar_genre WHERE nama_genre = ? ORDER BY 1,2;";
        
        try(Connection conn = connect();
            PreparedStatement pstmtSelectBukuBerdasarkanGenre = 
                    conn.prepareStatement(sqlSelectBukuBerdasarkanGenre)) {
            
            pstmtSelectBukuBerdasarkanGenre.setString(1,namaGenre);
            
            ResultSet rsSelectBukuBerdasarkanGenre = pstmtSelectBukuBerdasarkanGenre.executeQuery();
            
            while(rsSelectBukuBerdasarkanGenre.next()){
                String judul = rsSelectBukuBerdasarkanGenre.getString("judul");
                String pengarang = rsSelectBukuBerdasarkanGenre.getString("pengarang");
                int idxUrutanMasuk = rsSelectBukuBerdasarkanGenre.getInt("urutan_masuk");
                
                String sqlSelectGenresBerdasarkanBuku = "SELECT nama_genre, urutan_masuk "
                        + "FROM daftar_genre WHERE judul = ? AND pengarang = ? ORDER BY 2";
                
                PreparedStatement pstmtSelectGenresBerdasarkanBuku =
                        conn.prepareStatement(sqlSelectGenresBerdasarkanBuku);
                
                pstmtSelectGenresBerdasarkanBuku.setString(1,judul);
                pstmtSelectGenresBerdasarkanBuku.setString(2,pengarang);
                
                ResultSet rsSelectGenresBerdasarkanBuku = 
                        pstmtSelectGenresBerdasarkanBuku.executeQuery();
                
                while(rsSelectGenresBerdasarkanBuku.next()){
                    String genreBuku = rsSelectGenresBerdasarkanBuku.getString("nama_genre");
                    int urutanMasukGenreBuku = rsSelectGenresBerdasarkanBuku.getInt("urutan_masuk");
                    
                    if(urutanMasukGenreBuku > idxUrutanMasuk){
                        String sqlUpdateUrutanGenreBuku = "UPDATE daftar_genre "
                                + "SET urutan_masuk = ? WHERE judul = ?"
                                + " AND pengarang = ? AND nama_genre = ?;";
                        PreparedStatement pstmtUpdateUrutanGenreBuku =
                                conn.prepareStatement(sqlUpdateUrutanGenreBuku);
                        
                        pstmtUpdateUrutanGenreBuku.setInt(1, urutanMasukGenreBuku - 1);
                        pstmtUpdateUrutanGenreBuku.setString(2, judul);
                        pstmtUpdateUrutanGenreBuku.setString(3, pengarang);
                        pstmtUpdateUrutanGenreBuku.setString(4, genreBuku);
                        
                        int count = pstmtUpdateUrutanGenreBuku.executeUpdate();
                        
                    }
                    String sqlDeleteGenreBuku = "DELETE FROM daftar_genre "
                                    + "WHERE judul = ? AND  pengarang = ? AND nama_genre = ?;";
                            
                            PreparedStatement pstmtDeleteGenreBuku = 
                                    conn.prepareStatement(sqlDeleteGenreBuku);
                            
                            pstmtDeleteGenreBuku.setString(1, judul);
                            pstmtDeleteGenreBuku.setString(2, pengarang);
                            pstmtDeleteGenreBuku.setString(3, namaGenre);
                            
                            pstmtDeleteGenreBuku.executeUpdate();
                }
            }
            
            String sqlDeleteGenre = "DELETE FROM genre WHERE nama_genre = ?;";
            PreparedStatement pstmtDeleteBuku = conn.prepareStatement(sqlDeleteGenre);
            
            pstmtDeleteBuku.setString(1, namaGenre);
            
            int count2 = pstmtDeleteBuku.executeUpdate();
            
            if(count2 > -1){
                return true;
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return false;
    }

    public static boolean editBuku(String judul, String pengarang, String penerbit, 
            ArrayList<String> genres, int peringkat, int tahunTerbit, String sinopsis, 
            String status, String pathSampul) {
        String sqlUpdateBuku = "UPDATE buku SET penerbit = ?, peringkat = ?,"
                + " tahun_terbit = ?, sinopsis = ?, status = ?, path_sampul= ?"
                + " WHERE judul = ? AND pengarang = ?;";
 
        try (Connection conn = connect();
            PreparedStatement pstmtUpdateBuku = conn.prepareStatement(sqlUpdateBuku)) {
            if(penerbit.equals("Penerbit")) pstmtUpdateBuku.setString(1, "");
            else pstmtUpdateBuku.setString(1, penerbit);
            if(peringkat <= 0 || peringkat > 5) pstmtUpdateBuku.setInt(2, 3);
            else pstmtUpdateBuku.setInt(2, peringkat);
            if(tahunTerbit == 0) pstmtUpdateBuku.setString(3, "");
            else pstmtUpdateBuku.setInt(3, tahunTerbit);
            if(sinopsis.equals("Sinopsis . . .")) pstmtUpdateBuku.setString(4, "");
            else pstmtUpdateBuku.setString(4, sinopsis);
            pstmtUpdateBuku.setString(5, status);
            if(pathSampul.equals("")) pstmtUpdateBuku.setString(6, "defaultImage.jpg");
            else pstmtUpdateBuku.setString(6, pathSampul);
            pstmtUpdateBuku.setString(7, judul);
            pstmtUpdateBuku.setString(8, pengarang);
            
            int count = pstmtUpdateBuku.executeUpdate();
            
            if(count > 0){
                if(genres.size() > 0){
                    String sqlDeleteGenre = "DELETE from daftar_genre WHERE "
                            + "judul = ? AND pengarang = ?;";
                    PreparedStatement pstmtDeleteGenre = 
                            conn.prepareStatement(sqlDeleteGenre);
                    pstmtDeleteGenre.setString(1, judul);
                    pstmtDeleteGenre.setString(2, pengarang);
                    
                    int count3 = pstmtDeleteGenre.executeUpdate();
                    
                    if(count3 > -1){
                        String sqlInsertGenre = "INSERT INTO daftar_genre(judul,"
                                + "pengarang,nama_genre,urutan_masuk) "
                                + "VALUES(?,?,?,?);";
                        PreparedStatement pstmtInsertGenre = 
                                conn.prepareStatement(sqlInsertGenre);
                        pstmtInsertGenre.setString(1, judul);
                        pstmtInsertGenre.setString(2, pengarang);
                        int i = 1;

                        boolean isSuccessStatus = true;
                        for(String genre: genres){
                            pstmtInsertGenre.setString(3, genre);
                            pstmtInsertGenre.setInt(4, i);

                            int count2 = pstmtInsertGenre.executeUpdate();
                            if(count2 < 0){
                                isSuccessStatus = false;
                            } 
                            i++;
                        }
                        if(isSuccessStatus){
                            return true;
                        }
                    }
                }
                else{
                    return true;
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }
    
    
    public static boolean editGenre(String genreLama, String genreBaru) {
        String sqlUpdateGenre = "UPDATE genre SET nama_genre = ? WHERE nama_genre = ?;";
        
        try (Connection conn = connect();
            PreparedStatement pstmtUpdateGenre = conn.prepareStatement(sqlUpdateGenre)) {
            pstmtUpdateGenre.setString(1, genreBaru);
            pstmtUpdateGenre.setString(2, genreLama);
            
            int count = pstmtUpdateGenre.executeUpdate();
            
            if(count > -1){
                String sqlUpdateDaftarGenre = "UPDATE daftar_genre SET nama_genre = ? WHERE nama_genre = ?;";
                
                PreparedStatement pstmtUpdateDaftarGenre = conn.prepareStatement(sqlUpdateDaftarGenre);
                
                pstmtUpdateDaftarGenre.setString(1, genreBaru);
                pstmtUpdateDaftarGenre.setString(2, genreLama);
                
                int count2 = pstmtUpdateDaftarGenre.executeUpdate();
                
                if(count2 > -1){
                    return true;
                }
                return true;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }
    
    public static boolean editPeringkat(String judul, String pengarang, int peringkatBaru) {
        String sqlUpdatePeringkat = "UPDATE buku SET peringkat = ?"
                + " WHERE judul = ? AND pengarang = ?;";
 
        try (Connection conn = connect();
            PreparedStatement pstmtUpdatePeringkat = conn.prepareStatement(sqlUpdatePeringkat)) {
                if(peringkatBaru <= 0 || peringkatBaru > 5) pstmtUpdatePeringkat.setInt(2, 3);
                else pstmtUpdatePeringkat.setInt(1, peringkatBaru);
                pstmtUpdatePeringkat.setString(2, judul);
                pstmtUpdatePeringkat.setString(3, pengarang);

                int count = pstmtUpdatePeringkat.executeUpdate();

                if(count > 0){
                    return true;
                }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    public static String selectPassword(){
        String password = "";
        String sqlSelectPassword = "SELECT * FROM password;";
        try (Connection conn = connect();
             Statement stmtSelectPassword  = conn.createStatement();
             ResultSet rsSelectPassword = stmtSelectPassword.executeQuery(sqlSelectPassword)){
            
            // loop through the result set            
            if (rsSelectPassword.next()) {
                //ambil genre
                String teks = rsSelectPassword.getString("teks");
                password = teks;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
        return password;
    }

    public static boolean changePassword(char[] passwordBaru) {
        String sqlUpdatePassword = "UPDATE password SET teks = ?;";
 
        try (Connection conn = connect();
            PreparedStatement pstmtUpdatePassword = conn.prepareStatement(sqlUpdatePassword)) {
                String temp = "";
                for(char c:passwordBaru){
                    temp += c;
                }
                pstmtUpdatePassword.setString(1, temp);

                int count = pstmtUpdatePassword.executeUpdate();

                if(count > -1){
                    return true;
                }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    
}
