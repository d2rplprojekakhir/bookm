/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bookm_application;

import java.util.ArrayList;

/**
 *
 * @author Christnawan
 */
public class Buku {
    private String judul;
    private String pengarang;
    private String penerbit;
    private ArrayList<String> genres;
    private int peringkat;
    private int tahunTerbit;
    private String sinopsis;
    private String status;
    private String pathSampul;
    
    Buku(String judul,String pengarang, String penerbit, ArrayList<String> genres
        ,int peringkat, int tahunTerbit, String sinopsis, String status,String pathSampul){
        this.judul = judul;
        this.pengarang = pengarang;
        this.penerbit = penerbit;
        this.genres = genres;
        this.peringkat = peringkat;
        this.tahunTerbit = tahunTerbit;
        this.sinopsis = sinopsis;
        this.status = status;
        this.pathSampul = pathSampul;
    }
    
    public String getTranslatedStatus(){
        switch(this.status){
            case "1": return "Sedang";
            case "2": return "Sudah";
            case "0": 
            default: return "Belum";
        }
    }
    
    /**
     * @return the judul
     */
    public String getJudul() {
        return judul;
    }

    /**
     * @param judul the judul to set
     */
    public void setJudul(String judul) {
        this.judul = judul;
    }

    /**
     * @return the pengarang
     */
    public String getPengarang() {
        return pengarang;
    }

    /**
     * @param pengarang the pengarang to set
     */
    public void setPengarang(String pengarang) {
        this.pengarang = pengarang;
    }

    /**
     * @return the penerbit
     */
    public String getPenerbit() {
        return penerbit;
    }

    /**
     * @param penerbit the penerbit to set
     */
    public void setPenerbit(String penerbit) {
        this.penerbit = penerbit;
    }

    /**
     * @return the genres
     */
    public ArrayList<String> getGenres() {
        return genres;
    }

    /**
     * @param genres the genres to set
     */
    public void setGenres(ArrayList<String> genres) {
        this.genres = genres;
    }

    /**
     * @return the peringkat
     */
    public int getPeringkat() {
        return peringkat;
    }

    /**
     * @param peringkat the peringkat to set
     */
    public void setPeringkat(int peringkat) {
        this.peringkat = peringkat;
    }

    /**
     * @return the tahunTerbit
     */
    public int getTahunTerbit() {
        return tahunTerbit;
    }

    /**
     * @param tahunTerbit the tahunTerbit to set
     */
    public void setTahunTerbit(int tahunTerbit) {
        this.tahunTerbit = tahunTerbit;
    }

    /**
     * @return the sinopsis
     */
    public String getSinopsis() {
        return sinopsis;
    }

    /**
     * @param sinopsis the sinopsis to set
     */
    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the pathSampul
     */
    public String getPathSampul() {
        return pathSampul;
    }

    /**
     * @param pathSampul the pathSampul to set
     */
    public void setPathSampul(String pathSampul) {
        this.pathSampul = pathSampul;
    }
}
